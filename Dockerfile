FROM ubuntu:20.04

ENV DEBIAN_FRONTEND 'noninteractive'

RUN apt-get -y update &&  \
    apt-get install -y --no-install-recommends xorriso syslinux isolinux python3-jsonschema \
      p7zip-full wget ca-certificates unzip jq && \
    wget https://dl.min.io/client/mc/release/linux-amd64/mcli_20210526191926.0.0_amd64.deb && \
    dpkg -i mcli_20210526191926.0.0_amd64.deb && \
    wget -O "vault.zip" https://releases.hashicorp.com/vault/1.6.2/vault_1.6.2_linux_amd64.zip && \
    unzip vault.zip && \
    mv vault /usr/bin/ && \
    rm -f vault.zip && \
    rm -rf /var/lib/apt/lists/*
